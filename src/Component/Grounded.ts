import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { IsInCollar } from "../Outift/OutfitCtrl";
import { GetEffect } from "./EffectControl";

export function GroundedCtrl(mod: ModSDKModAPI) {
    mod.hookFunction('ChatRoomCanLeave', 0, (args, next) => {
        if (Player && IsInCollar(Player)) {
            if (GetEffect('Grounded')) return false;
        }
        return next(args);
    });
}