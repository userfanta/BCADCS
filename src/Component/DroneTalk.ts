import { IsTargetGroupInOutfit } from "../Outift/OutfitCtrl";


export function DroneTalkResult(player: Character, msg: string) {
    return `${player.MemberNumber} :: ${msg}`;
}

export function DroneTalk(player: Character, msg: string) {
    if (IsTargetGroupInOutfit(player, 'ItemNeck')) {
        return DroneTalkResult(player, msg);
    }
    return msg;
}