import { GetData } from "../Data";

export function SetEffect(effect: EffectType, turnOn: boolean) {
    if (GetData().Effects.includes(effect)) {
        if (!turnOn) {
            GetData().Effects = GetData().Effects.filter(_ => _ !== effect);
        }
    } else {
        if (turnOn) {
            GetData().Effects.push(effect);
        }
    }
}

export function FlipEffect(effect: EffectType) {
    if (GetData().Effects.includes(effect)) {
        GetData().Effects = GetData().Effects.filter(_ => _ !== effect);
    } else {
        GetData().Effects.push(effect);
    }
}

export function GetEffect(effect: EffectType) {
    return GetData().Effects.includes(effect);
}