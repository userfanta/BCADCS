export class FlowText {
    private _content: string
    private _index: number

    constructor(content: string) {
        this._content = content;
        this._index = 0;
    }

    set(content: string) {
        this._content = content;
    }

    progress(step: number) {
        this._index += step;
    }

    take(count: number) {
        const target = this._index - count;
        const start = Math.max(target, 0);

        if (target > this._content.length) this._index = 0;

        let ret = this._content.slice(start, target + count);
        if (ret.length < count) {
            if (target < 0)
                return ' '.repeat(count - ret.length) + ret;
            else return ret + ' '.repeat(count - ret.length);
        }
        return ret;
    }
}