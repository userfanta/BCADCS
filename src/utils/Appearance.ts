export function AppearanceUpdate(C: Character, Group?: string) {
    if (CurrentScreen !== 'ChatRoom') CharacterRefresh(C);
    else if (Group) ChatRoomCharacterItemUpdate(C, Group);
    else ChatRoomCharacterUpdate(C);
}

export function ColorCompare(lhs: string | string[], rhs: string | string[]) {
    if (typeof lhs === 'string') lhs = [lhs];
    if (typeof rhs === 'string') rhs = [rhs];
    if (lhs.length !== rhs.length) return false;

    for (let i = 0; i < lhs.length; i++) {
        if (lhs[i] !== rhs[i]) return false;
    }

    return true;
}

export function Color2Str(val: number) {
    let ret = val.toString(16);
    return ret.length < 2 ? '0' + ret : ret;
}