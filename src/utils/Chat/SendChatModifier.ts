import { ModSDKModAPI } from "bondage-club-mod-sdk";

type ChatModType = ((player: Character, msg: string) => string | undefined)

export class SendChatModifier {
    private _Modifiers: { priority: number, modifier: ChatModType }[] = [];

    register(priority: number, modifier: ChatModType) {
        this._Modifiers.push({ priority, modifier });
        this._Modifiers.sort((a, b) => b.priority - a.priority);
    }

    init(mod: ModSDKModAPI) {
        mod.hookFunction('ServerSend', 10, (args, next) => {
            if (Player && args[0] === 'ChatRoomChat' && (args[1] as any).Type === 'Chat') {
                let result = this._Modifiers.reduce((pv, cv) => {
                    if (pv) return cv.modifier(Player as Character, pv);
                    return undefined;
                }, (args[1] as any).Content as string | undefined);
                if (!result) return;
                else (args[1] as any).Content = result;
            };
            next(args);
        });
    }
}