import { ChatRoomLocalAction, ChatRoomSendAction } from "../utils/ChatMessages";
import CNSource, { ref_game_regex as CN_Ref } from "./CN";
import ENSource, { ref_game_regex as EN_Ref } from "./EN";

function GetStringSrc() {
    if (TranslationLanguage === 'CN') return CNSource
    else return ENSource
}

export function GetRefRegExp() {
    if (TranslationLanguage === 'CN') return CN_Ref;
    else return EN_Ref
}

export function GetString(key: keyof typeof CNSource, arg?: ({ key: string, target: string } | string)[]) {
    const src = GetStringSrc()[key];
    if (arg)
        return format(src, arg);
    else return src;
}

function format(src: string, arg: ({ key: string, target: string } | string)[]) {
    arg.forEach((v, id) => {
        if (typeof v === 'string') {
            src = src.replace(new RegExp(`\\{${id}\\}`, 'g'), v);
        } else {
            src = src.replace(new RegExp(`\\{${v.key}\\}`, 'g'), v.target);
        }
    });
    return src;
}

export function ChatRoomStdLocalActionFormat(key: keyof typeof CNSource, arg: ({ key: string, target: string } | string)[]) {
    ChatRoomStdLocalAction(GetString(key, arg));
}

export function ChatRoomStdSendActionFormat(key: keyof typeof CNSource, arg: ({ key: string, target: string } | string)[]) {
    ChatRoomStdSendAction(GetString(key, arg));
}

export function ChatRoomStdLocalAction(src: string) {
    ChatRoomLocalAction(`[ADCS] ${src}`);
}

export function ChatRoomStdSendAction(src: string) {
    ChatRoomSendAction(`[ADCS] ${src}`);
}