import { DroneName } from "../../../Contents";
import { GetData, WriteData } from "../../../Data";
import { ChatRoomStdSendAction } from "../../../Locale";
import { IsOwnedBy, IsSaotome, NotBlacklisted, Or } from "../../../utils/Criteria";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(?:export data)", "u"),
        validator: [NotBlacklisted, Or(IsOwnedBy, IsSaotome)],
        worker: (player: Character, sender: Character) => {
            let msg = LZString.compressToBase64(JSON.stringify(GetData()));
            ChatRoomStdSendAction(`${DroneName(player)} processing data...`)
            ServerSend("ChatRoomChat", { Content: msg, Type: "Whisper", Target: sender.MemberNumber });

            return undefined;
        }
    },
    {
        handler: new RegExp("^(?:import data)[\\p{P} ]+((?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?)", "u"),
        validator: [NotBlacklisted, Or(IsOwnedBy, IsSaotome)],
        worker: (player: Character, sender: Character, match: RegExpExecArray) => {
            let res = LZString.decompressFromBase64(match[1]);
            if (res === null) return;
            WriteData(JSON.parse(res));
            ChatRoomStdSendAction(`${DroneName(player)} data processed.`);

            return undefined;
        }
    },
]

export default CommandList;