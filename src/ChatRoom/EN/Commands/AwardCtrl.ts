import { DroneName } from "../../../Contents";
import { GetData, ServerStoreData } from "../../../Data";
import { IncreasePunish, IncreaseScore } from "../../../Component/ScoreControl";
import { queueShockAction } from "../../../Component/ShockProvider";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { CommandUnit } from "../../CommandBasics";
import { IsInCollar } from "../../../Outift/OutfitCtrl";
import { GetEffect, SetEffect } from "../../../Component/EffectControl";
import { ChatRoomStdSendAction } from "../../../Locale";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^good slave", "iu"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            ChatRoomStdSendAction(`${IncreaseScore()}。`);
            ServerStoreData();

            return undefined;
        }
    },
    {
        handler: new RegExp("^penalty", "iu"),
        validator: [AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            ChatRoomStdSendAction(`${IncreasePunish()}。`);
            ServerStoreData();

            return undefined;
        }
    },
    {
        handler: new RegExp("^award credit", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (GetData().ADCS.Punish > 0) {
                ChatRoomStdSendAction(`Penalty exists, ${DroneName(player)} cannot be awarded.`);
                return;
            }

            let v = Math.floor(GetData().ADCS.Score / 2);
            let bonus = Math.max(v * 5, v * 10 - 150, v * 15 - 600);

            if (player.Money) {
                if (typeof player.Money !== 'number')
                    player.Money = 0;

                GetData().ADCS.Score -= v;
                CharacterChangeMoney(player, bonus);
                ReputationProgress('Dominant', -v);
                ServerStoreData();
                ServerPlayerSync();
                ServerPlayerReputationSync();
            }
            ChatRoomStdSendAction(`${v} Bonus is converted to ${bonus} credit for ${DroneName(player)}, who becomes more submissive.`);
            return undefined;
        }
    },
    {
        handler: new RegExp("^shock punishment", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            queueShockAction(GetData().ADCS.Punish);
            ServerStoreData();
            ChatRoomStdSendAction(`${IncreasePunish()}。`);
            return undefined;
        }
    },
    {
        handler: new RegExp("^increase skill[\\p{P}\\s~]*(evasion|bondage|Self-bondage|willpower|lock-picking)", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (GetData().ADCS.Punish > 0) {
                ChatRoomStdSendAction(`Penalty exists, ${DroneName(player)} cannot be awarded.`);
                return;
            }

            let v = Math.floor(GetData().ADCS.Score / 2);
            let skill = result[1].toLowerCase();

            const SkillMap = new Map<string, string>([
                ['evasion', 'Evasion'],
                ['bondage', 'Bondage'],
                ['self-bondage', 'SelfBondage'],
                ['willpower', 'Willpower'],
                ['lock-picking', 'LockPicking'],
            ])

            let skType = SkillMap.get(skill) as string;
            GetData().ADCS.Score -= v;
            SkillProgress(skType, v);
            ServerStoreData();
            ServerPlayerSkillSync();

            ChatRoomStdSendAction(`System intervenes the mind of ${DroneName(player)}, ${v} Bonus is used in increasing the exp of ${skill}.`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(cancel )?(?:award full gear)", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                GetData().Effects = GetData().Effects.filter(_ => _ !== 'AwardFullGear');
            } else if (!GetData().Effects.includes('AwardFullGear')) {
                GetData().Effects.push('AwardFullGear');
            }
            ServerStoreData();
            ChatRoomStdSendAction(`Now ${DroneName(player)} in full gear for 1 hour ${GetData().Effects.includes('AwardFullGear') ? 'will' : 'won\'t'} get 1 bonus.`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(cancel |stop )?(?:punish orgasm)", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetEffect('PunishOrgasm', false);
            } else {
                SetEffect('PunishOrgasm', true);
            }
            ServerStoreData();
            ChatRoomStdSendAction(`${DroneName(player)} now ${GetEffect('PunishOrgasm') ? 'will' : 'won\'t'} be punished from orgasms.`);

            return undefined;
        }
    },
]

export default CommandList;