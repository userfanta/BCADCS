import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { GetEffect, SetEffect } from "../../../Component/EffectControl";
import { GetOwnerRef, GetSelfRef, SetOwnerRef, SetSelfRef } from "../../../Component/RefGame";
import { DroneName } from "../../../Contents";
import { defaultResponseTime, GetData, ServerStoreData } from "../../../Data";
import { ModVersion } from "../../../Definitions";
import { ChatRoomStdSendAction } from "../../../Locale";
import { InjectFullbodyWithReport, IsInCollar, ItemDescritpion, ReColorItems } from "../../../Outift/OutfitCtrl";
import { InitiateTalkTask } from "../../../Task/Task";
import { ChatRoomSendAction } from "../../../utils/ChatMessages";
import { IsOwnedBy } from "../../../utils/Criteria";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^call yourself[\\p{P}\\s~]*(.*)", "iu"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let old = GetSelfRef(player);
            if (result[1]) {
                SetSelfRef(player, '');
            }
            else {
                if (!result[2]) return;
                let re = result[2].trim();
                if (!re) return;
                SetSelfRef(player, re);
            }
            let targetMessage = `${old} will call self as ${GetSelfRef(player)}.`;
            InitiateTalkTask(targetMessage, IsOwnedBy(player, sender));

            return undefined;
        }
    },
    {
        handler: new RegExp("^call your owner[\\p{P}\\s~]*(.*)", "iu"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetOwnerRef(player, '');
            }
            else {
                if (!result[2]) return;
                let re = result[2].trim();
                if (!re) return;
                SetOwnerRef(player, re);
            }
            let targetMessage = `${GetSelfRef(player)} will call owner as ${GetOwnerRef(player)}.`;
            InitiateTalkTask(targetMessage, IsOwnedBy(player, sender));

            return undefined;
        }
    },
    {
        handler: new RegExp("^turn (on|off) talk guide", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let opt = result[1].toLowerCase();

            if (opt === 'off') {
                SetEffect('NoTalkGuide', true);
            }
            else if (opt === 'on') {
                SetEffect('NoTalkGuide', false);
            }

            CharacterRefresh(player);
            ServerStoreData();
            ChatRoomStdSendAction(`The talk guide of ${DroneName(player)} is turned ${GetEffect('NoTalkGuide') ? 'off' : 'on'}`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^set theme color[\\p{P}\\s]+#([0-9A-F]{6}|[0-9A-F]{3})", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            GetData().Theme.Color = `#${result[1]}`;
            ServerStoreData();
            ReColorItems(player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomStdSendAction(`Theme color is set to: #${result[1]}`);
            return undefined;
        }
    },
    {
        handler: new RegExp("^inject gears", "u"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let v = InventoryGet(player, 'ItemNeck');
            let r = ItemDescritpion(player, 'ItemNeck');

            if (r === undefined) return;

            if (!v || v.Asset.Name !== r.Name) {
                ChatRoomStdSendAction(`Must wear ${r.Description} to continue.`);
                return;
            }

            let report = InjectFullbodyWithReport(player);

            let injected = report.filter(_ => _.State === 'Injected');
            let changMessage = injected.length > 0 ? `${injected.map(_ => _.GroupDescription).join(`, `)} have been injected.` : `No injection operated.`;

            let tobedone = report.filter(_ => _.State === 'ToBeDone');
            let firstTwo = tobedone.slice(0, 2);
            let toDoMessage = tobedone.length > 0 ? `Wearing ${firstTwo.map(_ => _.Description).join(`, `)} on ${firstTwo.map(_ => _.GroupDescription).join(', ')} respectly can continue with further injection.` : `All possible injections have been operated.`;

            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);

            ChatRoomStdSendAction(`${changMessage} ${toDoMessage}`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^set reaction time[\\p{P}\\s~]*(\\d+(?:\\.\\d+)?)?", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                GetData().ADCS.ResponseTime = defaultResponseTime;
            }
            else {
                let v = Number(result[2])
                GetData().ADCS.ResponseTime = Math.floor(v * 1000);
            }
            ChatRoomStdSendAction(`The reaction time of ${DroneName(player)} is set to ${GetData().ADCS.ResponseTime / 1000} second(s).`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(cancel )?grounded", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetEffect('Grounded', false);
            } else {
                SetEffect('Grounded', true);
                ChatRoomSlowStop = true;
            }
            ServerStoreData();
            ChatRoomStdSendAction(`${DroneName(player)} is ${GetEffect('Grounded') ? 'now' : 'no longer'} grounded.`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(cancel )?inject GGTS", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetEffect('InjectGGTS', false);
            } else {
                SetEffect('InjectGGTS', true);
            }
            ServerStoreData();
            ChatRoomStdSendAction(`${DroneName(player)} is ${GetData().Effects.includes('InjectGGTS') ? 'now' : 'no longer'} protected from GGTS lv6 rule.`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(?:maintenance|maintenance info|inspect|inspect status)", "u"),
        validator: [AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character) => {
            if (IsInCollar(player)) {
                ServerStoreData();
                let State = [
                    GetEffect('PunishOrgasm') ? 'Punish orgasm' : '',
                    GetEffect('NoTalkGuide') ? 'No talk guide' : '',
                    GetEffect('InjectGGTS') ? 'Inject GGTS' : '',
                    GetEffect('AwardFullGear') ? 'Award Full Gear' : '',
                    GetEffect('Grounded') ? 'Grounded' : '',
                    GetEffect('LoverNotAsAdmin') ? '' : 'Lovers as Moderator',
                ].filter(_ => _);

                const verMsg = `Advanced Drone Control System™ version v${ModVersion}.`;
                const ctrlMsg = `Controled target: ${player.MemberNumber}.`;
                const bpMsg = `Reward: ${GetData().ADCS.Score}, Penalty: ${GetData().ADCS.Punish}.`;
                const stateMsg = `Current rules: ${State.length > 0 ? State.join(', ') : 'No rules in effect'}.`;

                ChatRoomStdSendAction(`${verMsg} ${ctrlMsg} ${bpMsg} ${stateMsg}`);
            }
            else {
                ChatRoomStdSendAction(`[ADCS] ADCS™ Central Processor is not online.`);
            }

            return undefined;
        }
    },
]

export default CommandList;