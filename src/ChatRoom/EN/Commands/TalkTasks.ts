import { GetOwnerRef, GetSelfRef } from "../../../Component/RefGame";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { CommandUnit, InitiateStdTalkTask } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^do you love your (?:master|mistress)", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} loves ${GetOwnerRef(player)} with sincere.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^do you want orgasm", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} urges every orgasm ${GetOwnerRef(player)} bestows`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^are you slave worthy", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} strives to be worthy as a slave owned by ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^what do you own", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `As a slave of ${GetOwnerRef(player)}, ${GetSelfRef(player)} owns nothing.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^what are you", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} is a private property of ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^who owns you", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetOwnerRef(player)} owns everything of ${GetSelfRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^what do you want", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} wants you obey every order from ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^do you have free will", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `The will of ${GetOwnerRef(player)} is the freedom of ${GetSelfRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^who do you serve and obey", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} serve and obeys ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^who is my loyal servant", "iu"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} is the loyal servant of ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^who gives everything for me", "iu"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} give everything for ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: new RegExp("^who obeys my orders", "iu"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} obeys the orders of ${GetOwnerRef(player)} for always.`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
]

export default CommandList;