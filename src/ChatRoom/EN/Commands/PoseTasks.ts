import { CheckPose, InitiatePoseTask } from "../../../Task/Task";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { BasicBonusCriteria, BasicPosePenaltyCriteria, CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^raise your hands?(?:[\\p{P}\\s]+(.+))?", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const target = ['Yoked', 'OverTheHead'];
            if (CheckPose(player, target)) return;
            InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));

            return result[1];
        }
    },
    {
        handler: new RegExp("^put your arms (behind|behind back|in front)(?:[\\p{P}\\s]+(.+))?", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const opt = result[1].toLowerCase();

            if (['behind', 'behind back'].includes(opt)) {
                const target = ['BackBoxTie', 'BackElbowTouch', 'BackCuffs'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['in front'].includes(opt)) {
                const target = ['BaseUpper'];
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
    {
        handler: new RegExp("^(stand|stand up|kneel|kneel down)(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const opt = result[1].toLowerCase();

            if (['stand', 'stand up'].includes(opt)) {
                const target = ['BaseLower', 'LegsClosed'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['kneel', 'kneel down'].includes(opt)) {
                const target = ['KneelingSpread', 'Kneel'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
    {
        handler: new RegExp("^(open|spread|close) your legs(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let opt = result[1].toLowerCase();
            if (['open', 'spread'].includes(opt)) {
                const target = ['Spread', 'KneelingSpread', 'BaseLower'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['close'].includes(opt)) {
                const target = ['LegsClosed', 'Kneel'];
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
]

export default CommandList;