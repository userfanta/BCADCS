import ActCmds from "./Commands/ActTasks"
import AdmCmds from "./Commands/Authority"
import AwdCmds from "./Commands/AwardCtrl"
import BindCmds from "./Commands/BodyBind"
import Misc from "./Commands/Miscs"
import PoseCmds from "./Commands/PoseTasks"
import SettCmds from "./Commands/Settings"
import TalkCmds from "./Commands/TalkTasks"

const CommandList = [
    ActCmds,
    AdmCmds,
    AwdCmds,
    BindCmds,
    Misc,
    PoseCmds,
    SettCmds,
    TalkCmds
].reduce((pv, cv) => { return cv.concat(pv) }, []);

export default CommandList;