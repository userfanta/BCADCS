import { GetOwnerRef, GetRef, GetSelfRef } from "../../Component/RefGame";
import { CharacterName } from "../../utils/Character";
import { InitiateTalkTaskWithDepVariant } from "../CommandBasics";


let areaItemCounter = new Map<string, number>();

let areaCounter = new Map<string, number>();

let whipCounter = 0;
let spankCounter = 0
let slapCounter = 0;

function counterToOrdinal(count: number) {
    const tens = count % 100;
    if (tens > 20 || tens < 10) {
        const ones = tens % 10;
        if (ones === 1) return `${count}st`
        if (ones === 2) return `${count}nd`
        if (ones === 3) return `${count}rd`
    }
    return `${count}th`
}

function InitiateTaskFromActivity(player: Character, sender: Character, act: { ActivityName: string, ActivityGroup: string }) {
    const pronoun = GetRef(player, sender, {
        Deprevated: `T`,
        Owner: `${GetOwnerRef(player)}, t`,
        Other: `${CharacterName(sender)}, t`
    });

    if (['Spank', 'Slap'].includes(act.ActivityName)) {
        if (act.ActivityGroup === 'ItemHead' && act.ActivityName === 'Slap') {
            slapCounter += 1;
            InitiateTalkTaskWithDepVariant(player, sender, `${pronoun}hank you for slapping ${GetSelfRef(player)} for the ${counterToOrdinal(slapCounter)} time.`);
        }
        else {
            spankCounter += 1;
            InitiateTalkTaskWithDepVariant(player, sender, `${pronoun}hank you for spanking ${GetSelfRef(player)} for the ${counterToOrdinal(spankCounter)} time.`);
        }
    }
    else if (act.ActivityName === 'SpankItem') {
        whipCounter += 1;
        InitiateTalkTaskWithDepVariant(player, sender, `${pronoun}hank you for whipping ${GetSelfRef(player)} for the ${counterToOrdinal(whipCounter)} time.`);
    }
}

export default InitiateTaskFromActivity;