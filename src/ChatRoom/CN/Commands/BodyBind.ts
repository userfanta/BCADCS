import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { DroneName } from "../../../Contents";
import { CheckOutfitParts, IsInCollar } from "../../../Outift/OutfitCtrl";
import { RestraintCtrl } from "../../../Outift/SetOutfit";
import { ChatRoomSendAction } from "../../../utils/ChatMessages";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(束缚|约束|解开|打开|取消)(双臂|双手|手臂|手部|双脚|双腿|腿部|四肢)(?:束缚)?", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let work = '闭合';
            let target = '手臂';

            let targetKey: (keyof typeof RestraintCtrl)[] = [];

            if (['双臂', '双手', '手臂', '手部'].includes(result[2])) {
                target = '手臂';
                targetKey = ['Arms'];
            }
            else if (['双脚', '双腿', '腿部'].includes(result[2])) {
                target = '腿部';
                targetKey = ['Legs'];
            }
            else if (['四肢'].includes(result[2])) {
                target = '四肢';
                targetKey = ['Arms', 'Legs'];
            }
            else return;

            let workKey: keyof typeof RestraintCtrl.Arms = 'TurnOn';

            if (['束缚', '约束'].includes(result[1])) {
                work = '闭合';
                workKey = 'TurnOn';
            }
            else if (['解开', '打开', '取消'].includes(result[1])) {
                work = '解开';
                workKey = 'TurnOff';
            }
            else return;

            let checkResult = CheckOutfitParts(player, targetKey);
            if (checkResult.length > 0) {
                ChatRoomSendAction(`[ADCS] 无法执行：${checkResult.join('、')}非ADCS系统束缚。`);
                return;
            }

            targetKey.forEach(_ => RestraintCtrl[_][workKey](player));
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 的${target}束缚已${work}。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^自由(?:活动|行动)", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let checkResult = CheckOutfitParts(player, ['ItemLegs', 'ItemFeet', 'ItemArms']);
            if (checkResult.length > 0) {
                ChatRoomSendAction(`[ADCS] 无法执行：${checkResult.join('、')}非ADCS系统束缚。`);
                return;
            }

            RestraintCtrl.Arms.TurnOff(player);
            RestraintCtrl.Legs.TurnOff(player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 的束缚已释放。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(打开|封闭|关闭)(视觉|听觉|贞操锁)", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetDesc = result[2];
            let targetKey: keyof typeof RestraintCtrl = (() => {
                switch (targetDesc) {
                    case '视觉': return 'Eyes';
                    case '听觉': return 'Ears';
                    case '贞操锁': return 'Chaste';
                }
                return "Eyes";
            })();

            let checkResult = CheckOutfitParts(player, [targetKey]);
            if (checkResult.length > 0) {
                ChatRoomSendAction(`[ADCS] 无法执行：${checkResult.join('、')}非ADCS系统束缚。`);
                return;
            }

            let workDesc = '打开'
            let workKey: keyof typeof RestraintCtrl.Arms = 'TurnOn';
            if (['打开'].includes(result[1])) {
                workDesc = '打开';
                workKey = 'TurnOn';
            }
            else if (['封闭', '关闭'].includes(result[1])) {
                workDesc = '关闭';
                workKey = 'TurnOff';
            }
            else return;

            RestraintCtrl[targetKey][workKey](player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 的${targetDesc}已${workDesc}。`);
            return undefined;
        }
    },
]

export default CommandList;