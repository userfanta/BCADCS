import { ChatRoomSendAction } from "../../.././utils/ChatMessages";
import { DroneName } from "../../../Contents";
import { GetData, ServerStoreData } from "../../../Data";
import { IncreasePunish, IncreaseScore } from "../../../Component/ScoreControl";
import { queueShockAction } from "../../../Component/ShockProvider";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { CommandUnit } from "../../CommandBasics";
import { IsInCollar } from "../../../Outift/OutfitCtrl";
import { GetEffect, SetEffect } from "../../../Component/EffectControl";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^做(?:的|地|得)很(?:棒|好)", "u"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            ChatRoomSendAction(`[ADCS] ${IncreaseScore()}。`);
            ServerStoreData();

            return undefined;
        }
    },
    {
        handler: new RegExp("^增加惩罚", "u"),
        validator: [AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            ChatRoomSendAction(`[ADCS] ${IncreasePunish()}。`);
            ServerStoreData();

            return undefined;
        }
    },
    {
        handler: new RegExp("^奖励存款", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (GetData().ADCS.Punish > 0) {
                ChatRoomSendAction(`[ADCS] ${DroneName(player)} 有惩罚点数，无法执行奖励。`);
                return;
            }

            let v = Math.floor(GetData().ADCS.Score / 2);
            let bonus = Math.max(v * 5, v * 10 - 150, v * 15 - 600);

            if (player.Money) {
                if (typeof player.Money !== 'number')
                    player.Money = 0;

                GetData().ADCS.Score -= v;
                CharacterChangeMoney(player, bonus);
                ReputationProgress('Dominant', -v);
                ServerStoreData();
                ServerPlayerSync();
                ServerPlayerReputationSync();
            }
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 的${v}奖励点数转换为${bonus}的存款，并因此更加顺从。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(?:实施)?电击惩罚", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            ChatRoomSendAction(`[ADCS] ${IncreasePunish()}。`);
            queueShockAction(GetData().ADCS.Punish);
            ServerStoreData();

            return undefined;
        }
    },
    {
        handler: new RegExp("^提升技能[\\p{P}\\s~]*(逃脱|捆绑|自缚|意志|开锁)", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (GetData().ADCS.Punish > 0) {
                ChatRoomSendAction(`[ADCS] ${DroneName(player)} 有惩罚点数，无法执行奖励。`);
                return;
            }

            let v = Math.floor(GetData().ADCS.Score / 2);
            let skill = result[1];

            const SkillMap = new Map<string, string>([
                ['逃脱', 'Evasion'],
                ['捆绑', 'Bondage'],
                ['自缚', 'SelfBondage'],
                ['意志', 'Willpower'],
                ['开锁', 'LockPicking'],
            ])

            let skType = SkillMap.get(skill) as string;
            GetData().ADCS.Score -= v;
            SkillProgress(skType, v);
            ServerStoreData();
            ServerPlayerSkillSync();

            ChatRoomSendAction(`[ADCS] 系统干涉了${DroneName(player)}的思维，消耗 ${v} 奖励点数提升${skill}的经验。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(取消)?(?:奖励持续穿戴(?:整齐|全套)(?:装备|束缚))", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                GetData().Effects = GetData().Effects.filter(_ => _ !== 'AwardFullGear');
            } else if (!GetData().Effects.includes('AwardFullGear')) {
                GetData().Effects.push('AwardFullGear');
            }
            ServerStoreData();
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 现在穿戴ADCS套装累积1小时${GetData().Effects.includes('AwardFullGear') ? '会' : '不会'}得到1点数。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(停止|取消|不再)?(?:惩罚高潮)", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetEffect('PunishOrgasm', false);
            } else {
                SetEffect('PunishOrgasm', true);
            }
            ServerStoreData();
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 现在${GetEffect('PunishOrgasm') ? '会' : '不会'}因为高潮受到惩罚。`);

            return undefined;
        }
    },
]

export default CommandList;