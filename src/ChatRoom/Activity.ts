import { ActivityDeconstruct } from ".././utils/ChatMessages";
import { ADCSActivityCheck } from "../Task/Task";
import { IsTargetGroupInOutfit } from "../Outift/OutfitCtrl";
import CNActTask from "./CN/ActivityTask"
import ENActTask from "./EN/ActivityTask"

export function ActivityHandle(player: Character, sender: Character, data: IChatRoomMessage) {
    if (data.Dictionary) {
        if (!IsTargetGroupInOutfit(player, 'ItemNeck')) return;
        let dd = ActivityDeconstruct(data.Dictionary);

        if (!dd) return;

        if (player.MemberNumber && dd.SourceCharacter.MemberNumber === player.MemberNumber) {
            ADCSActivityCheck(player, dd);
            return;
        }

        if (dd.TargetCharacter.MemberNumber === player.MemberNumber) {
            if (TranslationLanguage === 'CN') CNActTask(player, sender, dd);
            else ENActTask(player, sender, dd);
        }
    }
}

export function CanDoAnyOfActivities(player: Character, target: Character, activities: string[], groups?: string[]) {
    if (groups === undefined) groups = [
        'ItemFeet', 'ItemLegs', 'ItemVulva', 'ItemVulvaPiercings', 'ItemButt', 'ItemPelvis', 'ItemTorso',
        'ItemTorso2', 'ItemNipples', 'ItemBreast', 'ItemArms', 'ItemHands', 'ItemNeck', 'ItemNeckAccessories',
        'ItemNeckRestraints', 'ItemMouth', 'ItemMouth2', 'ItemMouth3', 'ItemHead', 'ItemNose', 'ItemEars',
        'ItemBoots'];

    if (activities.length === 0) return false;
    let allowedAct = ([] as string[]).concat(activities);

    groups.forEach(group => {
        if (allowedAct.length === 0) return;
        ActivityAllowedForGroup(target, group, false).forEach(act => {
            if (allowedAct.includes(act.Name))
                allowedAct = allowedAct.filter(_ => _ !== act.Name);
        })
    })

    return allowedAct.length !== activities.length;
}