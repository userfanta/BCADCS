import { IsModerator } from "../Component/Authority";
import { InitiateTalkTask } from "../Task/Task";

export interface CommandUnit {
    handler: RegExp;
    validator: ((self: Character, sender: Character) => boolean)[];
    worker: (player: Character, sender: Character, match: RegExpExecArray) => string | undefined;
}

export function InitiateTalkTaskWithDepVariant(player: Character, sender: Character, msg: string) {
    if (!PreferenceIsPlayerInSensDep() && IsModerator(player, sender)) {
        InitiateTalkTask(msg, true, player.CanTalk());
    }
    else {
        InitiateTalkTask(msg, (Math.random() > 0.2), false);
    }
}

export function InitiateStdTalkTask(player: Character, sender: Character, msg: string) {
    if (!(PreferenceIsPlayerInSensDep() && player.ImmersionSettings?.SenseDepMessages)) {
        InitiateTalkTaskWithDepVariant(player, sender, msg);
    }
}

export function BasicBonusCriteria(player: Character, sender: Character) {
    return IsModerator(player, sender) || (Math.random() > 0.2);
}

export function BasicPosePenaltyCriteria(player: Character, sender: Character, poses: string[]) {
    return IsModerator(player, sender) && poses.some(_ => player.CanChangeToPose(_));
}
