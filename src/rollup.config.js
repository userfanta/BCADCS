import typescript from "@rollup/plugin-typescript";
import progress from "rollup-plugin-progress";
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import { obfuscator } from 'rollup-obfuscator';
import copy from 'rollup-plugin-copy';
import path from 'path'


const config_d = {
    folder: "ADCS",
    input: "ADCS.ts",
    output: "main.js",
    loader: "loader.user.js",
}

const relative_dir = path.relative(".", __dirname).replace(/\\/g, "/");

const plugins_debug = [
    copy({
        targets: [
            { src: `${relative_dir}/${config_d.loader}`, dest: `public/${config_d.folder}` }
        ]
    }),
    progress({ clearLine: true }),
    resolve({ browser: true }),
    typescript({ tsconfig: `${relative_dir}/tsconfig.json`, inlineSources: true }),
    commonjs(),
]

const plugins = plugins_debug.concat([
    obfuscator({
        unicodeEscapeSequence: true,
    })]);

const config_default = {
    input: `${relative_dir}/${config_d.input}`,
    output: {
        file: `public/${config_d.folder}/${config_d.output}`,
        format: "iife",
        sourcemap: false,
        banner: ``,
    },
    treeshake: true
}
export default commandLineArgs => {
    if (commandLineArgs.configDebug === true) {
        return { ...config_default, plugins: plugins_debug };
    }
    return { ...config_default, plugins: plugins };
};