type EffectType = 'NoTalkGuide' | 'AwardPainInOrgasm'
    | 'AwardFullGear' | 'InjectGGTS'
    | 'PunishOrgasm' | 'BlockRemotes' | 'Grounded' | 'LoverNotAsAdmin';

interface SolidDataType {
    Theme: { Color: string },
    ActiveTime: { Time: number },
    FullGearTime: { Time: number, BoundTime: number },
    Orgasm: {
        OrgasmTimes: number,
        RuinedTimes: number,
        ResistTimes: number,
        EdgeTime: number,
    },
    Moderator: number[],
    ADCS: {
        Score: number,
        Punish: number,
        ResponseTime: number,
        SelfReference: string,
        OwnerReference: string,
    },
    Effects: EffectType[],
}

type ModDataType = Partial<SolidDataType>;

interface Window {
    BCADCS_Loaded?: boolean;
}
